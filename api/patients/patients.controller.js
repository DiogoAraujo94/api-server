const {
    getPatients,
    deletePatient,
    getPacientById,
    createPatient,
    updatePatient
} = require("./patients.service");


module.exports = {
    getPatients: (req, res) => {
        getPatients((err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    getPacientById: (req, res) => {
        id = req.params.pacientId;
        getPacientById(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    deletePatient: (req, res) => {
        const {id} = req.headers;
        deletePatient(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record Not Found" // ALTERAR AQUI -> MESMO QUE APAGUE ENTRA AQUI
                });
            }
            return res.json({
                success: 1,
                message: "doctor deleted successfully"
            });
        });
    },
    createPatient: (req, res) => {
        const {nome, contacto, genero, cidade, email, morada, postal, pais, nascimento, seguro, nif,
        fuma, cigarros, gravida, estomago, detalhe,halito, sensibilidade, gengivas, dentes, fio, ultima, queixas, tratamentos,
            alergias, medicacaoescolha,medicacao,coracaoescolha, coracao,infectaescolha, infecta, alteracoes, motivacao, sorriso,
        recomendacao,seguradora,profissao} = req.headers;

        createPatient(nome, contacto, genero, cidade, email, morada, postal, pais, nascimento, seguro, nif,
            fuma, cigarros, gravida, estomago, detalhe,halito, sensibilidade, gengivas, dentes, fio, ultima, queixas, tratamentos,
            alergias, medicacaoescolha,medicacao,coracaoescolha, coracao,infectaescolha, infecta, alteracoes, motivacao, sorriso,
            recomendacao,seguradora,profissao,(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "saved successfully"
            });
        });
    },

    updatePatient: (req, res) => {
        const {paciente, nome, contacto, genero, cidade, email, morada, postal, pais, nascimento, seguro, nif,
            fuma, cigarros, gravida, estomago, detalhe,halito, sensibilidade, gengivas, dentes, fio, ultima, queixas, tratamentos,
            alergias, medicacaoescolha,medicacao,coracaoescolha, coracao,infectaescolha, infecta, alteracoes, motivacao, sorriso,
        recomendacao,seguradora,profissao} = req.headers;

        updatePatient(paciente,nome, contacto, genero, cidade, email, morada, postal, pais, nascimento, seguro, nif,
            fuma, cigarros, gravida, estomago, detalhe,halito, sensibilidade, gengivas, dentes, fio, ultima, queixas, tratamentos,
            alergias, medicacaoescolha,medicacao,coracaoescolha, coracao,infectaescolha,infecta, alteracoes, motivacao, sorriso,
            recomendacao,seguradora,profissao,(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "saved successfully"
            });
        });
    },

};
