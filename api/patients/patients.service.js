const pool = require("../../config/database");
//Id, Nome, NIF, Contacto, email, MoradaFK,,Genero, Valor(?), Cidade

//MORADA -- id, rua, cidade, codigo postal, pais.


module.exports = {

    getPatients: callBack => {
        pool.query(
                `select *
      from pacientes p
      where p.deleted = 'N'`,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    getPacientById: (id, callBack) => {
        pool.query(
                `select *
      from pacientes p
      where id = ` + id,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    deletePatient: (id, callBack) => {
        pool.query(
                `update pacientes set deleted='Y' where id = ?;`,
            [
                id
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
    createPatient: (nome, contacto, genero, cidade, email, morada, postal, pais, nascimento, seguro, nif,
                    fuma, cigarros, gravida, estomago, detalhe,halito, sensibilidade, gengivas, dentes, fio, ultima, queixas, tratamentos,
                    alergias, medicacaoEscolha,medicacao,coracaoEscolha, coracao,infectaEscolha,infecta, alteracoes, motivacao, sorriso,
                    recomendacao,seguradora,profissao,callBack) => {
        pool.query(
            `CALL createPatient(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);`,
            [
                nome,
                contacto,
                genero,
                cidade,
                email,
                morada,
                postal,
                pais,
                new Date(nascimento),
                seguro,
                nif,
                fuma, cigarros, gravida, estomago, detalhe, halito, sensibilidade, gengivas, dentes, fio, ultima, queixas, tratamentos,
                alergias, medicacaoEscolha,medicacao, coracaoEscolha,coracao, infectaEscolha,infecta, alteracoes, motivacao, sorriso,
                recomendacao,
                seguradora,
                profissao
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
    updatePatient: (id, nome, contacto, genero, cidade, email, morada, postal, pais, nascimento, seguro, nif,
                    fuma, cigarros, gravida, estomago, detalhe, halito, sensibilidade, gengivas, dentes, fio, ultima, queixas, tratamentos,
                    alergias, medicacaoEscolha,medicacao, coracaoEscolha,coracao, infectaEscolha,infecta, alteracoes, motivacao, sorriso,
                    recomendacao,seguradora,profissao,callBack) => {
        pool.query(
            `CALL updatePatient(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);`,
            [
                id,
                nome,
                contacto,
                genero,
                cidade,
                email,
                morada,
                postal,
                pais,
                new Date(nascimento),
                seguro,
                nif,
                fuma, cigarros, gravida, estomago, detalhe, halito, sensibilidade, gengivas, dentes, fio, ultima, queixas, tratamentos,
                alergias, medicacaoEscolha,medicacao, coracaoEscolha,coracao, infectaEscolha,infecta, alteracoes, motivacao, sorriso,
                recomendacao,
                seguradora,
                profissao
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
};
