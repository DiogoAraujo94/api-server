const router = require("express").Router();
const {
  //createUser,
  //login,
  //logout,
  //getUserByUserId,
  getPatients,
    updatePatient,
    getPacientById,
  // getLocals,
   deletePatient,
    createPatient
} = require("./patients.controller");
router.get("/", getPatients);
// router.get("/specialties", getSpecialties);
// router.get("/locals", getLocals);
router.get("/:pacientId",getPacientById);
 router.post("/delete", deletePatient);
 router.post("/", createPatient);
 router.post("/update", updatePatient);

module.exports = router;
