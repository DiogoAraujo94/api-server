const router = require("express").Router();
const {
  //createUser,
  //login,
  //logout,
  //getUserByUserId,
  getProducts,
    getProductById,
    updateProduct,
  // getLocals,
   deleteProduct,
    createProduct
} = require("./products.controller");
router.get("/", getProducts);
router.get("/:productId",     getProductById,);
// router.get("/specialties", getSpecialties);
// router.get("/locals", getLocals);
 router.post("/delete", deleteProduct);
 router.post("/", createProduct);
 router.post("/update", updateProduct);

module.exports = router;
