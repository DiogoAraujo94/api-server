const {
    getProducts,
    deleteProduct,
    getProductById,
    createProduct,
    updateProduct
} = require("./products.service");

module.exports = {
    getProducts: (req, res) => {
        getProducts((err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    getProductById: (req, res) => {
        id = req.params.productId;
        getProductById(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    deleteProduct: (req, res) => {
        const {id} = req.headers;
        deleteProduct(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record Not Found" // ALTERAR AQUI -> MESMO QUE APAGUE ENTRA AQUI
                });
            }
            return res.json({
                success: 1,
                message: "product deleted successfully"
            });
        });
    },
    createProduct: (req, res) => {
        const {codigo, nome, descricao, especialidade, valor, desconto, cadeira, notas} = req.headers;
        var desconto2 = desconto == '' ? null : desconto;

        createProduct(codigo, nome, descricao, especialidade, valor, desconto2, cadeira, notas, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
        });

        return res.json({
            success: 1,
            message: "saved successfully"
        });
    },

    updateProduct: (req, res) => {
        const {produto, codigo, nome, descricao, especialidade, valor, desconto, cadeira, notas} = req.headers;

        var desconto2 = desconto == '' ? null : desconto;

        updateProduct(produto, codigo, nome, descricao, especialidade, valor, desconto2, cadeira, notas, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "saved successfully"
            });
        });
    },

};
