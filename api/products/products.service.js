const pool = require("../../config/database");
//Id, Nome, NIF, Contacto, email, MoradaFK,,Genero, Valor(?), Cidade

//MORADA -- id, rua, cidade, codigo postal, pais.


module.exports = {

    getProducts: callBack => {
        pool.query(
                `select *
      from produtos p
      where p.deleted = 'N'`,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },

    getProductById: (id, callBack) => {
        pool.query(
                `select *
      from produtos p
      where p.id = ` + id,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    getSpecialties: callBack => {
        pool.query(
                `select id, name from specialties`,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    getLocals: callBack => {
        pool.query(
                `select id, name from locals`,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    deleteProduct: (id, callBack) => {
        pool.query(
                `update produtos set deleted='Y' where id = ?;`,
            [
                id
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
    createProduct: (codigo, nome, descricao, especialidade, valor, desconto, cadeira, notas, callBack) => {
        pool.query(
            `CALL createProduct(?,?,?,?,?,?,?,?);`,
            [
                codigo,
                nome,
                descricao,
                especialidade,
                valor,
                desconto,
                cadeira,
                notas
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
    updateProduct: (id, codigo, nome, descricao, especialidade, valor, desconto, cadeira, notas, callBack) => {
        pool.query(
            `CALL updateProduct(?,?,?,?,?,?,?,?,?);`,
            [
                id,
                codigo,
                nome,
                descricao,
                especialidade,
                valor,
                desconto,
                cadeira,
                notas
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
};
