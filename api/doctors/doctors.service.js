const pool = require("../../config/database");
//Id, Nome, NIF, Contacto, email, MoradaFK,,Genero, Valor(?), Cidade

//MORADA -- id, rua, cidade, codigo postal, pais.


module.exports = {

    getDoctors: (user, clinica, callBack) => {
        if (user === 'GESTOR') {
            pool.query(
                    `select d.id,d.nome,d.fotografia,d.especialidade,d.clinica
      from medicos d
      where d.deleted = 'N' and d.clinica = '` + clinica + `'`,
                [],
                (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                }
            );
        } else {
            pool.query(
                    `select d.id,d.nome,d.fotografia,d.especialidade
      from medicos d
      where d.deleted = 'N'`,
                [],
                (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                }
            );
        }
    },
    getDoctorById: (id, callBack) => {
        pool.query(
                `select d.id,d.nome,d.fotografia,d.especialidade,d.clinica
      from medicos d
      where id = ` + id,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    getDoctorsByClinic: (clinica, especialidades, callBack) => {
        pool.query(
                `select * from medicos where clinica = ? and especialidade in ? and deleted = 'N'`,
            [
                clinica,
                [especialidades]
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    deleteDoctor: (id, callBack) => {
        pool.query(
                `update medicos set deleted='Y' where id = ?;`,
            [
                id
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
    createDoctor: (nome, fotografia, especialidade, clinica, callBack) => {
        pool.query(
            `CALL createDoctor(?,?,?,?);`,
            [
                fotografia,
                nome,
                especialidade,
                clinica
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
    updateDoctor: (id, nome, especialidade, clinica, callBack) => {
        pool.query(
            `CALL updateDoctor(?,?,?,?);`,
            [
                id,
                nome,
                especialidade,
                clinica
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
    updatePhoto: (id, fotografia, callBack) => {
        pool.query(
            `CALL updatePhoto(?,?);`,
            [
                id,
                fotografia
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
};
