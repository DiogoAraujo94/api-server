const {
    getDoctors,
    deleteDoctor,
    getDoctorById,
    getDoctorsByClinic,
    createDoctor,
    updateDoctor,
    updatePhoto
} = require("./doctors.service");

module.exports = {
    getDoctors: (req, res) => {
        const { user, clinica} = req.headers;
        getDoctors(user,clinica,(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    getDoctorById: (req, res) => {
        id = req.params.doctorId;
        getDoctorById(id,(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    getDoctorsByClinic: (req, res) => {
        const { clinica, especialidades } = req.headers
        getDoctorsByClinic(clinica,JSON.parse(especialidades),(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    deleteDoctor: (req, res) => {
        const { id } = req.headers;
        deleteDoctor(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record Not Found" // ALTERAR AQUI -> MESMO QUE APAGUE ENTRA AQUI
                });
            }
            return res.json({
                success: 1,
                message: "doctor deleted successfully"
            });
        });
    },
    createDoctor: (req, res) => {
        const {nome, fotografia, especialidade,clinica} = req.headers;

        createDoctor(nome, fotografia, especialidade,clinica,(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "saved successfully"
            });
        });
    },

    updateDoctor: (req, res) => {
        const {doutor, nome,especialidade,clinica} = req.headers;

        updateDoctor(doutor,nome, especialidade,clinica,(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "saved successfully"
            });
        });
    },

    updatePhoto: (req, res) => {
        const {doutor,fotografia} = req.headers;

        updatePhoto(doutor,fotografia,(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "saved successfully"
            });
        });
    },

};
