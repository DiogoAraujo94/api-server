const router = require("express").Router();
const {
    //createUser,
    //login,
    //logout,
    //getUserByUserId,
    getDoctors,
    getDoctorsByClinic,
    updateDoctor,
    updatePhoto,
    getDoctorById,
    // getLocals,
    deleteDoctor,
    createDoctor
} = require("./doctors.controller");
router.get("/", getDoctors);
router.get("/clinic", getDoctorsByClinic);
// router.get("/specialties", getSpecialties);
// router.get("/locals", getLocals);
router.get("/:doctorId", getDoctorById);
router.post("/delete", deleteDoctor);
router.post("/", createDoctor);
router.post("/update", updateDoctor);
router.post("/photo", updatePhoto);

module.exports = router;
