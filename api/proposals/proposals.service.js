const pool = require("../../config/database");
//Id, Nome, NIF, Contacto, email, MoradaFK,,Genero, Valor(?), Cidade

//MORADA -- id, rua, cidade, codigo postal, pais.


module.exports = {

    getProposals: (email, user,clinica, callBack) => {
        if (user === 'ADMIN') {
            pool.query(
                    `select * from propostas p where p.deleted = 'N'`,
                [],
                (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                }
            );
        } else if (user === 'GESTOR') {
            pool.query(
                `select * from propostas p where p.deleted = 'N' and p.clinicaNome = '` + clinica + `'`,
                [],
                (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                }
            );
        } else {
            pool.query(
                    `select * from propostas p where p.deleted = 'N' and p.useremail = '` + email + `'`,
                [],
                (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                }
            );
        }
    },
    getProposalById: (id, callBack) => {
        pool.query(
                `select * 
        from propostas p
        where p.id = ` + id,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    deleteProposal: (id, valor, paciente, callBack) => {
        pool.query(
            `CALL deleteProposal(?,?,?);`,
            [
                id,
                valor,
                paciente
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },

    saveState: (proposta, metodo, estado, callBack) => {
        pool.query(
            `CALL saveState(?,?,?);`,
            [
                proposta,
                metodo,
                estado
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
    createProposal: (
        pacienteNome,
        contacto,
        email,
        morada,
        postal,
        cidade,
        nacionalidade,
        metodo,
        estado,
        cadeira,
        clinica,
        clinicaNome,
        pac,
        total,
        quantities,
        usernome,
        useremail,
        userfoto,
        usertele,
        potencial,
        nif,
        antes,
        depois,
        descontoTotal,
        consultas,
        pdf,
        metodos,
        mensalidade,
        taeg,
        callBack) => {
        pool.query(
            `CALL createProposal(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);`,
            [
                pacienteNome,
                contacto,
                email,
                morada,
                postal,
                cidade,
                nacionalidade,
                metodo,
                estado,
                cadeira,
                clinica,
                clinicaNome,
                pac,
                total,
                quantities,
                usernome,
                useremail,
                userfoto,
                usertele,
                potencial,
                nif,
                antes,
                depois,
                descontoTotal,
                pdf,
                consultas,
                metodos,
                mensalidade,
                taeg

            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },

    updateProposal: (proposta,
                     pacienteNome,
                     contacto,
                     email,
                     morada,
                     postal,
                     cidade,
                     nacionalidade,
                     metodo,
                     estado,
                     cadeira,
                     clinica,
                     clinicaNome,
                     patient,
                     total,
                     quantities,
                     potencial,
                     nif,
                     antes,
                     depois,
                     descontototal,
                     consultas,
                     metodos,
                     pdf,
                     mensalidade,
                     taeg,
                     callBack) => {

        if (antes !== '') {
            pool.query(`CALL updateAntes(?,?);`,
                [
                    id,
                    antes
                ],
                (error, results4, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    console.log(results4)

                }
            );
        }

        if (depois !== '') {
            pool.query(`CALL updateDepois(?,?);`,
                [
                    id,
                    depois
                ],
                (error, results2, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    console.log(results2)
                }
            );
        }
        pool.query(
            `CALL updateProposal(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);`,
            [
                proposta,
                pacienteNome,
                contacto,
                email,
                morada,
                postal,
                cidade,
                nacionalidade,
                metodo,
                estado,
                cadeira,
                clinica,
                clinicaNome,
                patient,
                total,
                quantities,
                potencial,
                nif,
                descontototal,
                consultas,
                metodos,
                pdf,
                mensalidade,
                taeg
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
};

