const router = require("express").Router();
const {
    //createUser,
    //login,
    //logout,
    //getUserByUserId,
    getProposals,
    updateProposal,
    getProposalById,
    saveState,
    deleteProposal,
    createProposal
} = require("./proposals.controller");
router.get("/", getProposals);
// router.get("/specialties", getSpecialties);
// router.get("/locals", getLocals);
router.get("/:proposalId", getProposalById);
router.post("/state", saveState);
router.post("/delete", deleteProposal);
router.post("/", createProposal);
router.post("/update", updateProposal);

module.exports = router;
