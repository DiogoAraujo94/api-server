const {
    getProposals,
    deleteProposal,
    getProposalById,
    createProposal,
    updateProposal,
    saveState
} = require("./proposals.service");

module.exports = {
    getProposals: (req, res) => {
    const {email, user, clinica} = req.headers;
        getProposals(email, user, clinica, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    getProposalById: (req, res) => {
        id = req.params.proposalId;
        getProposalById(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    deleteProposal: (req, res) => {
        const {id, valor, paciente} = req.headers;
        deleteProposal(id, valor, paciente, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record Not Found" // ALTERAR AQUI -> MESMO QUE APAGUE ENTRA AQUI
                });
            }
            return res.json({
                success: 1,
                message: "proposal deleted successfully"
            });
        });
    },
    saveState: (req, res) => {
        const {proposta, metodo, estado} = req.headers;
        saveState(proposta, metodo, estado, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record Not Found" // ALTERAR AQUI -> MESMO QUE APAGUE ENTRA AQUI
                });
            }
            return res.json({
                success: 1,
                message: "state saved successfully"
            });
        });
    },
    createProposal: (req, res) => {
        const {
            patient,
            pacientenome,
            contacto,
            email,
            morada,
            postal,
            cidade,
            nacionalidade,
            metodo,
            estado,
            cadeira,
            clinica,
            clinicanome,
            total,
            quantities,
            usernome,
            useremail,
            userfoto,
            usertele,
            potencial,
            nif,
            antes,
            depois,
            descontototal,
            pdf,
            consultas,
            metodos,
            mensalidade,
            taeg
        } = req.headers;

        createProposal(
            pacientenome,
            contacto,
            email,
            morada,
            postal,
            cidade,
            nacionalidade,
            metodo,
            estado,
            cadeira,
            clinica,
            clinicanome,
            patient,
            total,
            quantities,
            usernome,
            useremail,
            userfoto,
            usertele,
            potencial,
            nif,
            antes,
            depois,
            descontototal,
            consultas,
            pdf,
            metodos,
            mensalidade,
            taeg,
            (err, results) => {
                if (err) {
                    console.log(err);
                    return;
                }
                return res.json({
                    success: 1,
                    message: "saved successfully"
                });
            });
    },

    updateProposal: (req, res) => {
        const {
            proposta,
            patient,
            pacientenome,
            contacto,
            email,
            morada,
            postal,
            cidade,
            nacionalidade,
            metodo,
            estado,
            cadeira,
            clinica,
            clinicanome,
            total,
            quantities,
            potencial,
            nif,
            antes,
            depois,
            descontototal,
            consultas,
            metodos,
            pdf,
            mensalidade,
            taeg
        } = req.headers;

        updateProposal(
            proposta,
            pacientenome,
            contacto,
            email,
            morada,
            postal,
            cidade,
            nacionalidade,
            metodo,
            estado,
            cadeira,
            clinica,
            clinicanome,
            patient,
            total,
            quantities,
            potencial,
            nif,
            antes,
            depois,
            descontototal,
            consultas,
            metodos,pdf,
            mensalidade,
            taeg,
            (err, results) => {
                if (err) {
                    console.log(err);
                    return;
                }
                return res.json({
                    success: 1,
                    message: "saved successfully"
                });
            });
    },

};
