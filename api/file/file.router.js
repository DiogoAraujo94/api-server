require("dotenv").config();
var config = require('../../config.json');
const multer = require("multer");
const router = require("express").Router();
const sharp = require('sharp');
const fs = require ('fs');
const aws = require('aws-sdk');
const uuid = require('uuid');

aws.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID, 
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
})

// To use in future
//const { checkToken } = require("../../auth/token_validation");

const fileFilter = function(req, file, cb) {
    const allowedType = ["image/jpeg","image/png","image/gif"];

    if (!allowedType.includes(file.mimetype)) {
        const error = new Error("Wrong file type");
        error.code = "LIMIT_FILE_TYPES";
        return cb(error, false);
    }
    cb(null, true);
}

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

router.post("/upload", upload.single("file"), async (req, res) => {
    const s3 = new aws.S3();
    const now = Date.now();
    var awsS3BucketPath = "http://angelsmile-photos.s3-website-eu-west-1.amazonaws.com/";
    // if (req.params.originalS3Path === "HOME") {
    //     awsS3BucketPath = "";
    // } else {
    //     awsS3BucketPath = req.params.originalS3Path + "/";
    // }

    try {
        // const buffer = await sharp(req.file.buffer)
        // //.resize(300)
        // //.toFile(`./static/${req.file.originalname}`);
        // .toBuffer();
        const s3res = await s3.upload({
            Bucket: "angelsmile-photos",
            //Key: `${req.file.originalname}`
            //Key: `${awsS3BucketPath}-${now}${req.file.originalname}`,
            Key: `${req.file.originalname ==="blob" ? uuid.v4() : req.file.originalname}`,
            Body: req.file.buffer,
            ACL: "public-read",
            ContentType: req.file.mimetype,
        }).promise();

        // fs.unlink(req.file.path, () => {
            //res.json({ file: `/static/${req.file.originalname}`})
            res.json({ 
                originalName: req.file.originalname,
                file: s3res.Location
            });
        // })
    } catch(err) {
        res.status(422).json({err});
    }
});

module.exports = router;
