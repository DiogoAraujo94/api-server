const router = require("express").Router();
const {
  //createUser,
  //login,
  //logout,
  //getUserByUserId,
  getClinics,
    updateClinic,
    getClinicById,
  // getLocals,
   deleteClinic,
    createClinic
} = require("./clinics.controller");
router.get("/", getClinics);
// router.get("/specialties", getSpecialties);
// router.get("/locals", getLocals);
router.get("/:clinicId",getClinicById);
 router.post("/delete", deleteClinic);
 router.post("/", createClinic);
 router.post("/update", updateClinic);

module.exports = router;
