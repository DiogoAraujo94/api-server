const {
    getClinics,
    deleteClinic,
    getClinicById,
    createClinic,
    updateClinic
} = require("./clinics.service");

module.exports = {
    getClinics: (req, res) => {
        const { user, clinica} = req.headers;
        getClinics(user,clinica,(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    getClinicById: (req, res) => {
        id = req.params.clinicId;
        getClinicById(id,(err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    deleteClinic: (req, res) => {
        const { id } = req.headers;
        deleteClinic(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record Not Found" // ALTERAR AQUI -> MESMO QUE APAGUE ENTRA AQUI
                });
            }
            return res.json({
                success: 1,
                message: "doctor deleted successfully"
            });
        });
    },
    createClinic: (req, res) => {
        const {nome, morada, contacto} = req.headers;

        createClinic(nome, morada, contacto, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "saved successfully"
            });
        });
    },

    updateClinic: (req, res) => {
        const {clinica, nome, morada, contacto} = req.headers;

        updateClinic(clinica,nome, morada, contacto, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "saved successfully"
            });
        });
    },

};
