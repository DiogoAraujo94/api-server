const pool = require("../../config/database");
//Id, Nome, NIF, Contacto, email, MoradaFK,,Genero, Valor(?), Cidade

//MORADA -- id, rua, cidade, codigo postal, pais.


module.exports = {

    getClinics: (user, clinica, callBack) => {
        if (user === 'GESTOR') {
            pool.query(
                `select c.id,c.nome,c.morada,c.contacto
      from clinicas c
      where c.deleted = 'N' and c.nome = '` + clinica + `'`,
                [],
                (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                }
            );
        } else {
            pool.query(
                    `select c.id,c.nome,c.morada,c.contacto
      from clinicas c
      where c.deleted = 'N'`,
                [],
                (error, results, fields) => {
                    if (error) {
                        callBack(error);
                    }
                    return callBack(null, results);
                }
            );
        }
    },
    getClinicById: (id, callBack) => {
        pool.query(
                `select c.id,c.nome,c.morada,c.contacto
      from clinicas c
      where id = ` + id,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    deleteClinic: (id, callBack) => {
        pool.query(
                `update clinicas set deleted='Y' where id = ?;`,
            [
                id
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
    createClinic: (nome, morada, contacto, callBack) => {
        pool.query(
            `CALL createClinic(?,?,?);`,
            [
                nome,
                morada,
                contacto
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
    updateClinic: (id, nome, morada, contacto, callBack) => {
        pool.query(
            `CALL updateClinic(?,?,?,?);`,
            [
                id,
                nome,
                morada,
                contacto
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                console.log(results)
                return callBack(null, results[0]);
            }
        );
    },
};
