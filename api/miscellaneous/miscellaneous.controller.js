const {
    getLocals,
  } = require("./miscellaneous.service");
  
  module.exports = {
    getLocals: (req, res) => {
      getLocals((err, results) => {
        if (err) {
          console.log(err);
          return;
        }
        return res.json({
          success: 1,
          data: results
        });
      });
    },
  };
  