const router = require("express").Router();
const {
  getLocals,
} = require("./miscellaneous.controller");
router.get("/getLocals", getLocals);

module.exports = router;
