const pool = require("../../config/database");

module.exports = {

  getLocals: callBack => {
    pool.query(
      `select * from locals`,
      [],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results);
      }
    );
  },
};
