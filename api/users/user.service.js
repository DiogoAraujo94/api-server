const pool = require("../../config/database");

module.exports = {
    create: (data, callBack) => {
        pool.query(
                `insert into users(username, nome, email, telefone, pass, credenciais,foto) 
                values(?,?,?,?,?,?,?)`,
            [
                data.username,
                data.nome,
                data.email,
                data.telefone,
                data.pass,
                data.credenciais,
                data.foto
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    getUserByUserEmail: (email, callBack) => {
        pool.query(
                `select * from users where email = ?`,
            [email],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
    getUserByUserId: (id, callBack) => {
        pool.query(
                `select id,username,nome,email,telefone,foto,clinica from users where id = ?`,
            [id],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
    getUsers: callBack => {
        pool.query(
                `select id,username,nome,email,telefone,credenciais,foto,clinica from users`,
            [],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results);
            }
        );
    },
    getUser: (id, callBack) => {
        pool.query(
                `select * from users where id = ?`,
            [id],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
    updateUser: (data, callBack) => {
        pool.query(
                `update users set username=?, nome=?, email=?, telefone=?, pass=?, foto=? where id = ?`,
            [
                data.username,
                data.nome,
                data.email,
                data.telefone,
                data.pass,
                data.id,
                data.foto
            ],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    },
    deleteUser: (data, callBack) => {
        pool.query(
                `delete from users where id = ?`,
            [data.id],
            (error, results, fields) => {
                if (error) {
                    callBack(error);
                }
                return callBack(null, results[0]);
            }
        );
    }
};
