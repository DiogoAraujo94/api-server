const {hash} = require("bcrypt");
const {
    create,
    getUserByUserEmail,
    getUserByUserId,
    getUsers,
    getUser,
    updateUser,
    deleteUser
} = require("./user.service");
const {hashSync, genSaltSync, compareSync, compare} = require("bcrypt");
const {sign} = require("jsonwebtoken");

module.exports = {
    createUser: (req, res) => {
        const body = req.headers;
        // const salt = genSaltSync(10);
        console.log(req.body);
        // hash(body.pass, 8, function (err, hash) {
        //     if (err)
        //         return res.json({error: true});
        //     console.log('body: ', body);
        //     console.log('hash: ', hash);
        //     body.pass = hash;
        create(body, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: "Database connection error"
                });
            }
            return res.status(200).json({
                success: 1,
                data: results
            });
        });
    },
    login: (req, res) => {
        console.log(req.headers);
        console.log(req.client);
        console.log(req.body);
        console.log(req.route);
        console.log(req.headers['x-forwarded-for'] || req.connection.remoteAddress);

        const body = req.headers;
        getUserByUserEmail(body.email, (err, results) => {
            if (err) {
                console.log(err);
            }
            if (!results) {
                return res.json({
                    success: 0,
                    data: "Invalid email or password"
                });
            }
            const result = body.pass === results.pass;
            if (result) {
                results.pass = undefined;
                const jsontoken = sign({result: results}, process.env.JWT_KEY, {
                    expiresIn: "1h"
                });
                return res.json({
                    success: 1,
                    message: "login successfully",
                    token: jsontoken,
                    email: results.email,
                    username: results.username,
                    name: results.nome,
                    telefone: results.telefone,
                    foto: results.foto,
                    clinica: results.clinica,
                    credenciais: results.credenciais
                });
            } else {
                return res.json({
                    success: 0,
                    data: "Invalid email or password"
                });
            }
        });
    },
    logout: (req, res) => {
        return res.json({
            success: 1,
            data: "User logged out"
        });
    },
    getUserByUserId: (req, res) => {
        const id = req.params.id;
        getUserByUserId(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record not Found"
                });
            }
            results.password = undefined;
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    getUsers: (req, res) => {
        getUsers((err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    getUser: (req, res) => {
        console.log(req.decoded.result.id);
        const id = req.decoded.result.id;
        getUser(id, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record not Found"
                });
            }
            results.password = undefined;
            return res.json({
                success: 1,
                data: results
            });
        });
    },
    updateUsers: (req, res) => {
        const body = req.body;
        const salt = genSaltSync(10);
        body.password = hashSync(body.password, salt);
        updateUser(body, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            return res.json({
                success: 1,
                message: "updated successfully"
            });
        });
    },
    deleteUser: (req, res) => {
        const data = req.body;
        deleteUser(data, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: "Record Not Found" // ALTERAR AQUI -> MESMO QUE APAGUE ENTRA AQUI
                });
            }
            return res.json({
                success: 1,
                message: "user deleted successfully"
            });
        });
    }
};
