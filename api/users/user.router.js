const router = require("express").Router();
const {checkToken} = require("../../auth/token_validation");
const {
    createUser,
    login,
    logout,
    getUserByUserId,
    getUsers,
    getUser,
    updateUsers,
    deleteUser
} = require("./user.controller");
router.get("/", getUsers);
router.get("/user", getUser);
router.post("/", createUser);
router.get("/:id", getUserByUserId);
router.post("/login", login);
router.post("/logout", logout);
router.patch("/", updateUsers);
router.delete("/", deleteUser);

module.exports = router;
