require("dotenv").config();
const logger = require('./utils/logger').logger;

var config = require('./config.json');
console.log("    /\                    | |  / ____|         (_) |");
console.log("   /  \   _ __   __ _  ___| | | (___  _ __ ___  _| | ___\"");
console.log("  / /\ \ | '_ \ / _` |/ _ \ |  \___ \| '_ ` _ \| | |/ _ \\");
console.log(" / ____ \| | | | (_| |  __/ |  ____) | | | | | | | |  __/ ");
console.log("/_/    \_\_| |_|\__, |\___|_| |_____/|_| |_| |_|_|_|\___|\"");
console.log("                  __/ |\"");
console.log("                 |___/\"");
console.log("     /\         (_)\"");
console.log("    /  \   _ __  _\"");
console.log("   / /\ \ | '_ \| |\"");
console.log("  / ____ \| |_) | |\"");
console.log(" /_/    \_\ .__/|_|\"");
console.log("          | |\"");
console.log("          |_|\"");

const express = require("express");
const path = require('path');
const app = express();
//app.use("/static", express.static(path.join(__dirname, "static")));
//
// console.log("Initializing articles api...");
// const articlesRouter = require("./api/articles/article.router");
console.log("Initializing upload files api...");
const fileRouter = require("./api/file/file.router");
logger.info("Initializing users auth api...");
const userRouter = require("./api/users/user.router");
console.log("Initializing patients api...");
const patientsRouter = require("./api/patients/patients.router");
console.log("Initializing products api...");
const productsRouter = require("./api/products/products.router");
console.log("Initializing doctors api...");
const doctorsRouter = require("./api/doctors/doctors.router");
console.log("Initializing clinics api...");
const clinicsRouter = require("./api/clinics/clinics.router");
console.log("Initializing proposals api...");
const proposalsRouter = require("./api/proposals/proposals.router");
// console.log("Initializing miscellaneous api...");
// const miscellaneousRouter = require("./api/miscellaneous/miscellaneous.router");
//
console.log("Setting up CORS...");
var cors = require('cors')
app.use(cors())

app.use(express.json());


console.log("Setting up routers...");
app.use("/api/users", userRouter);
// app.use("/api/articles", articlesRouter);
app.use("/api/file", fileRouter);
app.use("/api/patients", patientsRouter);
app.use("/api/products", productsRouter);
app.use("/api/doctors", doctorsRouter);
app.use("/api/clinics", clinicsRouter);
app.use("/api/proposals", proposalsRouter);
// app.use("/api/miscellaneous", miscellaneousRouter);


app.use(function (err, req, res, next) {
    if (err.code === "LIMIT_FILE_TYPES") {
        res.status(422).json({error: "Only images are allowed"});
        return;
    }
    if (err.code === "LIMIT_FILE_SIZE") {
        res.status(422).json({error: `Too large. Max size is ${config.MAX_IMG_SIZE / 1000}Kb`});
        return;
    }
});

console.log("Defining server port...");
const port = process.env.PORT || 4000;

console.log("Starting...");
app.listen(port, () => {
    console.log("Server up and running on port :", port);
});


