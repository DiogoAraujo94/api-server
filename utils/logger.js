const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');


const logger = new winston.createLogger({
    transports: [
        new DailyRotateFile({
            filename: './application.log',
            datePattern: 'yyyy-MM-dd.',
            prepend: true,
            localTime: true,
            level: process.env.NODE_ENV === 'development' ? 'debug' : 'info',
        })
    ]
});


module.exports = {
    logger
};
